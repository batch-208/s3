package com.b208.postApp.services;

import com.b208.postApp.models.Post;
import com.b208.postApp.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Service is added so that springboot would recognize that business logic is implemented in this layers
@Service
public class PostServiceImpl implements PostService{
    //We're going to create an instance of the postRepository that us persistent within our springboot app
    //This is so we can use the pre-defined methods for database manipulation for our table so long as its autowired
    @Autowired
    private PostRepository postRepository;

    public void createPost(Post post) {
        //When createPost is used from our services, we will be able to pass a Post class object and then, using the .save() method of our postRepository we will be able to insert a new row into our posts table.
        postRepository.save(post);
    }

    public Iterable<Post> getPosts(){
        //returns all records from our table
        return postRepository.findAll();
    }

    public void updatePost(Long id, Post post){
        //post parameter here is actually the request object passed from the controller.
        //because post parameter is actually the request body converted as a post class object, it has the methods from a post class object
        //Therefore, getTitle() will actually get the value of the title property from the request body.
//        System.out.println("This is the id passed as path variable");
//        System.out.println(id);

        //findById() retrieves that matches the passed id
        Post postForUpdating = postRepository.findById(id).get();

//        System.out.println("This is the found data using the id");
//        System.out.println(postForUpdating.getTitle());
//
//        System.out.println("This is the request body passed from the controller");
//        System.out.println(post.getTitle());

        //The found post title will be updated with the title of the request body as post
        postForUpdating.setTitle(post.getTitle());

        //The found post content will be updated with the content of the request body as post
        postForUpdating.setContent(post.getContent());

        //save the updates to our data
        postRepository.save(postForUpdating);


    }

    public void deletePost(Long id){
        //Delete the record that matches the passed id
        postRepository.deleteById(id);
    }
}
