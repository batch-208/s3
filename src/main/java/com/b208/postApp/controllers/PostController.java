package com.b208.postApp.controllers;

import com.b208.postApp.models.Post;
import com.b208.postApp.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handle all HTTP responses
@RestController
//Enables cross origin resource request
//CORS - cross origin resource sharing is the ability to allow or disallow applications to share resources from each other. With this, we can allow or disallow clients to access our API
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //map web requests to controller methods via @RequestMapping
    //@RequestMapping(value = "/posts", method = RequestMethod.POST)
    @PostMapping("/posts")
    //ResponseEntity is an object that represents and contains the whole HTTP response, the actual message, the status code.
    //@RequestBody automatically converts JSON client input into our desired object.
    //PostMapping maps post method requests into an endpoint
    public ResponseEntity<Object> createPost(@RequestBody Post post){

        //@RequestBody converts the JSON input as the desired class of the parameter
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());

        //Use the createPost() method from our service and pass our post object
        postService.createPost(post);

        /*
        RequestBody(JSON) -> Post post object in the controller -> postService.createPost(post) -> postRepository.save(post)
        */

        return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    }

    @GetMapping("/posts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody Post post){
        //System.out.println(id);
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());

        //Pass the id and the request body as post to our postService method
        postService.updatePost(id, post);

        return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@PathVariable Long id){

        postService.deletePost(id);

        return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);

    }
}
